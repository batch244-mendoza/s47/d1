console.log("Hello, Feb!");

// For selecting elements, we will be using the document.querySelector()
// Syntax: document.querySelector("htmlElement")
// document - refers to the whole page
// querySelector - used to select a specific object(HTML element) from the document/webpage

	// The query selector () takes a string input that is formatted like a CSS selector when applying the styles
const txtFirstName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name");
const spanFullName = document.querySelector("#span-full-name");

txtLastName.addEventListener("keyup", (event) => {

	let fullNameResult = txtFirstName.value.concat("   ", txtLastName.value)
	spanFullName.innerHTML = fullNameResult;
})

